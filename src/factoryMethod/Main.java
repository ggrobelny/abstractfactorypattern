package factoryMethod;

import factoryMethod.Transport.TransportType;

public class Main {

    public static void main(String[] args) {
	System.out.println("Transport towarow bedzie trwal: ");
	// teraz wywoluje metody, ktore uzupelnia mi informacje po : dwukropku
    AbstractFactory cargoFactory = FactoryProvider.getFactory(FactoryMode.CARGO);
    // tworze fabryke cargoFactory, w klasie FactoryProvider wywoluje przez getFactory
    // i jako mode podaje CARGO
    cargoFactory.getTransport(TransportType.PLANE).process();
    // nastepnie fabryka tworzy odpowiedni srodek transportu poprzez
    // wywolanie metody getTransport, a na niej wywoluje metode process
    // ktora zwroci nam informacje o czasie trwanie danego transportu, w tym przypadku samolot
    System.out.println("Transport osobowy bedzie trwal: ");
    AbstractFactory passengerFactory = FactoryProvider.getFactory(FactoryMode.PASSENGER);
    passengerFactory.getTransport(TransportType.PLANE).process();
    // moja proba
    System.out.println("Transport zwierzat bedzie trwal: ");
    AbstractFactory animalFactory = FactoryProvider.getFactory(FactoryMode.ANIMAL);
    animalFactory.getTransport(TransportType.PLANE).process();

    }
}
