package factoryMethod;

public class FactoryProvider {
    // klasa zwraca odpowiednia fabryke
    static AbstractFactory getFactory(FactoryMode mode) {
        if (FactoryMode.PASSENGER == mode) {
            return new PassengerFactory();
        } else if (FactoryMode.CARGO == mode) {
            return new CargoFactory();
        } else if (FactoryMode.ANIMAL == mode) {
            return new AnimalFactory();
        }
        return null;
        // czyli jak FactoryMode bedzie rowny PASSENGER  tworze
        // PassengerFactory
        // jesli CARGO
        // to zwracam CargoFactory
    }
}
