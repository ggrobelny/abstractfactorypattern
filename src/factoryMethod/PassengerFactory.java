package factoryMethod;

import factoryMethod.Transport.*;

public class PassengerFactory extends AbstractFactory{
    @Override
    Transport getTransport(TransportType transportType) {
        // bede zwracac obiekt transport
        Transport transport;
        switch (transportType) {
            case BUS_TRUCK:
                transport = new PassengerBus();
                break;
            case SHIP:
                transport = new PassengerShip();
                break;
            case PLANE:
                transport = new PassengerPlane();
                break;
            default:
                transport = null;
        }
        // i zwracam obiekt transport, jak nadmienilem w linii 8
        return transport;
    }
}
